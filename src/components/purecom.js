import React, { PureComponent } from 'react'

 class PureCom extends PureComponent {
    render() {
        return (
            <div>
                <h1> {this.props.name}</h1>
            </div>
        )
    }
}

export default PureCom
