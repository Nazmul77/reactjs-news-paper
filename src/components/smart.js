import React, { Component } from 'react'
import Dump from './dump'
const message=[
    'Muhammad Nazmul Hossain',
    'Muhammad Rahman',
    'Muhammad Kamal',
    'Fahmida Akter',
    'Ardites Bangladesh Limited',
    'Brain Station23',
    'Nano it Bangladesh'
]

export default class Smart extends Component {
    state= {
        msg: message[0]
    }
    componentDidMount(){
        setInterval(() => {
            const randomIndex= Math.floor(Math.random()* message.length);
            this.setState({
                msg: message[randomIndex]
            })
        }, 1*1000);
    }
    render() {
        return (
            <div>
                <h3>I am Smart</h3>
                <Dump msg={this.state.msg}/>
            </div>
        )
    }
}
