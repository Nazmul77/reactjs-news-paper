import React, { Component } from 'react'
import Header from '../components/header'
import newsCategory from '../news/index'
import NewsList from '../components/newsList'
import Loading from '../components/loading'
import Pagination from '../components/pagination'
import axios from 'axios';
// import ComponentDidMount from '../lifeCycle/componentDidMount'



// const fakeNews=[
//     {
//         tittle: 'Tittle',
//         content: 'Content',
//         url: 'https://linktonews.com',
//         urlToImage: 'https://linktoimage.com',
//         publishedAT: 'published date and time',
//         source: {
//             name: 'CNN'
//         },
//         },
//         {
//             title: 'Tittle',
//             content: 'Content',
//             url: 'https://linktonews.com',
//             urlToImage: 'https://linktonewsimage.com',
//             publishedAt: 'published date and time',
//             source: {
//                 name: 'CNN'
//             },
//         }
// ]
// const URL= 'https://jsonplaceholder.typicode.com/users' 

// axios.get(URL).then(res=>{
//     console.log(res);
// })

// const user={
//     name: 'Muhammad Nazmul Hossain',
//     email: 'mdnazmulhossainnn@gmail.com',
//     username: 'muhammadnazmul'
// }
// axios.post(URL,user).then((res)=>{
//     console.log(res);
// });






export default class App extends Component {

    state={
        news:[],
        category: newsCategory.technology
    }   
    
    changeCatagory=(category)=>{
        this.setState({category})
    }

   componentDidMount(){
       
    
    // const url=`${process.env.REACT_APP_NEWS_URL}?apikey=${process.env.REACT_APP_NEWS_API_KEY}&catagory=technology&pageSize=5`;
    const url=`http://newsapi.org/v2/top-headlines?country=us&category=business&apiKey=aab1176555f84f419e6aad673db381bf`
    console.log(url);
    axios.get(url)
    .then((response)=>{
      this.setState({
          news: response.data.articles,
      })
    })
    .catch((e)=>{
        console.log(e);
    })
}
    render() {
        return (
            <div className='container'>
                <div className='row'>
                <div className='col-sm- offset-md-3'>
                  
                    <Header category={newsCategory.technology} changeCatagory={this.changeCatagory}/>
                    <div className='d-flex'>
                        <p className='text-black-50'>
                            About {0} results found
                        </p>
                        <p className='text-black-50 ml-auto'>
                            {1} page of {100}
                        </p>
                    </div>
                    <NewsList news={this.state.news}/>
                    <Pagination/>
                    <Loading/>
                   
                </div>
                </div>
                
            </div>
        )
    }
}
 