 const newsCategory={
    technology: 'technology',
    science: 'science',
    business: 'business',
    entertainment: 'entertainment',
    health: 'health',
    sports: 'sports',
};
export default newsCategory
